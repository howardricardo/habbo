const character = [
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "B", "W", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "B", "B", "G", "B", "G", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "G", "H", "B", "G", "H", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "J", "B", "G", "J", "H", "G", "G", "H", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "J", "G", "J", "G", "G", "J", "J", "G", "H", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["B", "G", "J", "J", "J", "J", "G", "G", "K", "H", "B", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "J", "J", "G", "J", "J", "J", "B", "B", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["B", "J", "G", "J", "J", "J", "J", "B", "K", "J", "J", "J", "J", "J", "J", "J", "J", "J", "G", "G", "G", "G", "G", "G", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["B", "J", "J", "J", "J", "J", "B", "K", "K", "K", "J", "J", "J", "J", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "J", "J", "H", "B", "K", "K", "K", "K", "K", "K", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "J", "J", "B", "K", "K", "K", "K", "K", "K", "H", "H", "G", "G", "G", "G", "G", "G", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "B", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "G", "G", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "K", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "K", "K", "K", "K", "K", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "K", "K", "K", "J", "J", "H", "H", "H", "H", "K", "K", "K", "K", "K", "K", "K", "J", "J", "J", "J", "G", "G", "G", "G", "G", "G", "G", "J", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "K", "J", "J", "J", "H", "H", "K", "K", "K", "K", "K", "K", "K", "K", "K", "K", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "K", "J", "J", "J", "H", "H", "K", "K", "K", "K", "K", "K", "K", "K", "H", "H", "H", "G", "G", "G", "H", "H", "H", "H", "H", "H", "G", "G", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "J", "J", "J", "J", "J", "K", "K", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "H", "H", "G", "G", "G", "G", "G", "H", "G", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "J", "J", "J", "J", "K", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "H", "H", "G", "G", "B", "B", "B", "B", "B", "G", "H", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "J", "J", "J", "K", "K", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "G", "G", "B", "B", "S", "B", "S", "S", "S", "B", "H", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "K", "J", "J", "J", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "H", "G", "G", "B", "B", "S", "S", "B", "D", "E", "E", "S", "B", "H", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "B", "K", "J", "J", "J", "K", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "G", "G", "B", "B", "S", "S", "S", "S", "B", "E", "B", "B", "S", "B", "H", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "J", "J", "J", "K", "K", "K", "K", "K", "H", "H", "H", "H", "H", "G", "B", "B", "S", "B", "B", "B", "S", "S", "B", "E", "B", "B", "S", "B", "H", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "J", "J", "K", "K", "K", "K", "K", "K", "H", "H", "H", "H", "G", "B", "S", "S", "B", "D", "E", "E", "S", "S", "B", "E", "B", "S", "S", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "B", "J", "K", "K", "K", "K", "K", "J", "J", "J", "J", "G", "B", "S", "S", "S", "B", "E", "B", "B", "S", "S", "A", "B", "S", "S", "S", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "K", "K", "K", "K", "J", "J", "J", "J", "G", "B", "S", "S", "S", "S", "S", "E", "B", "B", "S", "S", "S", "B", "S", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "B", "K", "K", "K", "J", "J", "J", "G", "B", "S", "S", "S", "S", "S", "S", "E", "B", "S", "S", "S", "S", "A", "B", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "B", "B", "K", "J", "J", "J", "G", "B", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "B", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "B", "B", "K", "J", "J", "J", "G", "B", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "B", "B", "A", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "B", "B", "J", "J", "B", "J", "J", "G", "B", "S", "A", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "Z", "Z", "S", "S", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "X", "B", "B", "B", "B", "J", "J", "G", "B", "S", "Z", "A", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "X", "X", "B", "B", "B", "B", "B", "B", "B", "S", "S", "B", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "B", "B", "S", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "B", "S", "S", "B", "S", "S", "S", "S", "S", "S", "S", "B", "B", "B", "S", "S", "A", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "X", "B", "S", "S", "B", "S", "S", "S", "S", "S", "S", "S", "S", "S", "S", "A", "B", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "C", "C", "C", "B", "X", "B", "B", "B", "X", "X", "B", "S", "S", "B", "S", "S", "S", "S", "S", "S", "S", "S", "A", "B", "V", "V", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "C", "C", "B", "C", "X", "X", "V", "V", "B", "X", "X", "B", "S", "S", "B", "B", "Z", "Z", "Z", "Z", "Z", "B", "B", "B", "V", "X", "V", "B", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "C", "B", "C", "X", "X", "X", "X", "V", "V", "B", "X", "X", "B", "S", "S", "S", "B", "B", "B", "B", "B", "B", "V", "B", "V", "X", "X", "X", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "C", "B", "C", "X", "X", "X", "X", "X", "V", "V", "B", "X", "X", "B", "B", "S", "S", "S", "S", "S", "B", "V", "V", "B", "V", "X", "X", "X", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "B", "C", "B", "C", "X", "X", "X", "X", "X", "X", "X", "V", "V", "B", "X", "X", "X", "B", "S", "S", "S", "B", "V", "V", "X", "B", "X", "X", "X", "X", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "B", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "V", "V", "B", "B", "B", "X", "B", "S", "S", "B", "V", "V", "X", "B", "X", "X", "X", "X", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "B", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "X", "V", "V", "V", "B", "B", "B", "B", "B", "V", "V", "X", "X", "B", "X", "X", "X", "X", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "V", "V", "B", "V", "V", "V", "V", "V", "V", "X", "X", "B", "X", "X", "X", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "B", "V", "V", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "X", "C", "B", "X", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "X", "B", "X", "X", "X", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "X", "B", "X", "X", "X", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "X", "B", "X", "X", "X", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "B", "X", "B", "X", "X", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "B", "X", "B", "X", "X", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "B", "X", "X", "X", "X", "X", "X", "X", "B", "B", "B", "X", "X", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "B", "X", "B", "X", "X", "X", "X", "X", "X", "X", "X", "X", "C", "B", "C", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "B", "X", "B", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "B", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "B", "B", "B", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "C", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "X", "X", "V", "B", "X", "X", "X", "X", "X", "X", "X", "X", "X", "V", "B", "X", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "X", "C", "C", "V", "B", "X", "X", "X", "X", "X", "X", "X", "X", "X", "V", "B", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "C", "C", "V", "B", "V", "X", "X", "X", "X", "X", "X", "X", "X", "V", "V", "B", "S", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "X", "C", "V", "V", "B", "V", "X", "X", "X", "X", "X", "X", "V", "V", "V", "B", "B", "S", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "C", "C", "V", "B", "V", "V", "V", "X", "X", "X", "V", "V", "V", "V", "B", "B", "S", "A", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "C", "X", "X", "X", "X", "C", "B", "V", "V", "X", "V", "V", "B", "V", "V", "V", "V", "V", "V", "V", "V", "V", "B", "B", "W", "B", "A", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "B", "X", "X", "X", "X", "C", "B", "V", "V", "C", "B", "B", "V", "V", "V", "V", "V", "V", "V", "V", "B", "B", "H", "B", "W", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "S", "B", "B", "X", "X", "C", "B", "V", "V", "C", "V", "V", "V", "V", "V", "V", "V", "V", "B", "B", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "S", "S", "S", "B", "B", "C", "B", "V", "C", "V", "V", "V", "V", "V", "V", "B", "B", "B", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "S", "A", "S", "S", "S", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "H", "H", "H", "B", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "S", "B", "S", "A", "S", "S", "A", "B", "K", "K", "J", "J", "J", "H", "H", "H", "H", "B", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "B", "A", "B", "S", "B", "S", "Z", "S", "B", "K", "K", "J", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "B", "B", "A", "B", "S", "B", "S", "B", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "B", "B", "Z", "B", "B", "B", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "A", "B", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "J", "H", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "B", "B", "H", "H", "H", "H", "H", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "H", "B", "H", "H", "H", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "H", "B", "H", "B", "B", "I", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "H", "H", "B", "B", "B", "U", "B", "I", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "H", "B", "B", "B", "O", "O", "B", "U", "B", "I", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "K", "J", "J", "J", "J", "H", "H", "H", "H", "H", "B", "B", "B", "O", "I", "U", "O", "B", "U", "O", "U", "B", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "K", "J", "J", "J", "J", "H", "H", "H", "B", "B", "I", "B", "O", "I", "U", "B", "U", "I", "O", "Y", "O", "U", "B", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "J", "J", "J", "H", "B", "B", "B", "U", "B", "I", "B", "I", "B", "U", "U", "Y", "U", "O", "Y", "U", "U", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "I", "B", "B", "B", "B", "B", "O", "O", "B", "U", "B", "I", "B", "I", "U", "Y", "Y", "Y", "Y", "U", "Y", "Y", "U", "B", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "O", "I", "U", "U", "I", "B", "U", "I", "O", "B", "U", "O", "I", "B", "O", "I", "U", "Y", "Y", "Y", "Y", "Y", "U", "B", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "O", "I", "B", "O", "I", "B", "U", "I", "B", "I", "I", "O", "Y", "O", "U", "B", "O", "I", "I", "U", "U", "U", "I", "O", "B", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "O", "I", "B", "O", "I", "B", "I", "I", "Y", "U", "U", "Y", "I", "U", "B", "O", "I", "I", "U", "U", "U", "I", "B", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "O", "I", "B", "I", "U", "Y", "Y", "Y", "Y", "U", "Y", "Y", "U", "B", "B", "B", "B", "B", "B", "B", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "O", "U", "O", "I", "Y", "Y", "Y", "Y", "Y", "Y", "I", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "I", "U", "O", "O", "I", "I", "U", "I", "O", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "I", "I", "U", "U", "U", "U", "U", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "B", "B", "B", "B", "B", "B", "B", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
    ["W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W", "W"],
]

const pixer = (pixel, className) => {
    pixel = document.createElement("div")
    pixel.classList.add(className)
    document.getElementById("root").appendChild(pixel)
}

character.forEach(line => line.forEach(pixel => {
    if (pixel === "W") {
        pixer(pixel, "yellow")
    } 
    if (pixel === "B") {
        pixer(pixel, "black")
    }
    if (pixel === "G") {
        pixer(pixel, "gray1")
    }
    if (pixel === "H") {
        pixer(pixel, "gray2")
    }
    if (pixel === "J") {
        pixer(pixel, "gray3")
    }
    if(pixel === "K") {
        pixer(pixel, "gray4")
    } 
    if (pixel === "S") {
        pixer(pixel, "skin")
    }
    if(pixel === "A") {
        pixer(pixel, "skin2")
    }
    if (pixel === "Z") {
        pixer(pixel, "skin3")
    }
    if (pixel === "E") {
        pixer(pixel, "eye")
    }
    if (pixel === "D") {
        pixer(pixel, "eye2")
    }
    if (pixel === "X") {
        pixer(pixel, "jaquet")
    }
    if (pixel === "C") {
        pixer(pixel, "jaquet2")
    }
    if (pixel === "V") {
        pixer(pixel, "jaquet3")
    }
    if (pixel === "Y") {
        pixer(pixel, "foot")
    }
    if (pixel === "U") {
        pixer(pixel, "foot2")
    }
    if (pixel === "I") {
        pixer(pixel, "foot3")
    }
    if (pixel === "O") {
        pixer(pixel, "foot4")
    }
}))